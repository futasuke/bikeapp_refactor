import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity
} from 'react-native';
import axios from 'axios';
import { useFocusEffect } from '@react-navigation/native';
import { observer, inject } from "mobx-react";

import ActivityModal from '../ActivityModal';
import Post from './HomeContentComponent/Post';

const HomeContent = inject("store")(observer((props) => {
    const [showspinner, setShowSpinner] = useState(false);
    const [postdata, setPostData] = useState([]);

    // UseEffect hoook as componentdidmount
    useEffect(() => {
        console.log(props);
        getPostData();
    }, [])

    // Get post lists from backend
    function getPostData() {
        setShowSpinner(true);
        axios.get(props.store.ip + "/graphql?query={postAll{id,content,created_at,user{fullname,picturePath},comment{content,user{fullname}}}}")
            .then(function (response) {
                console.log(response);
                setShowSpinner(false);
                setPostData(response.data.data.postAll);
                // checkState();
            }).catch(function (error) {
                setShowSpinner(false);
                console.log(error);
            })
    }

    // function checkState() {
    //     console.log(postdata);
    // }

    function showSpinnerModal() {
        if (showspinner == true) {
            return (
                <ActivityModal />
            )
        }
    }

    // Call Post Component
    // Render each of them using array map
    function renderPost() {

        return postdata.map(function (data, index) {
            return (
                <Post key={index} postData={data} navigation={props.navigation}/>
                // <View>
                //     <Text>Test</Text>
                // </View>
            )
        })
    }

    return (
        <View style={styles.contentContainer}>

            {/* Activity Indicator */}
            {showSpinnerModal()}

            {renderPost()}

        </View>
    )
}));

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        alignItems: "center"
    },
    postContainer: {
        backgroundColor: "white",
        width: "90%",
        padding: 10,
        borderRadius: 20,
        flexDirection: "row"
    }
})

export default HomeContent;