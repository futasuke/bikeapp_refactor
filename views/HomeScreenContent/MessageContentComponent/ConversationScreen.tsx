import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    TextInput,
    RefreshControl,
    Keyboard,
    ActivityIndicator
} from 'react-native';
import axios from 'axios';
import { observer, inject } from "mobx-react";
import ActivityModal from '../../ActivityModal';

const ConversationScreen = inject("store")(observer((props) => {
    const [showspinner, setShowSpinner] = useState(false)
    const [sendMessage, setSendMessage] = useState("")
    const [messages, setMessages] = useState([])
    const [refreshing, setRefreshing] = useState(false)

    useEffect(() => {
        getMessages();
    }, [])

    function showSpinnerModal() {
        if (showspinner == true) {
            return (
                <ActivityModal />
            )
        }
    }

    // Get messages for this chat room
    function getMessages() {
        setShowSpinner(true)
        axios.get(props.store.ip + `/graphql?query={chatroomdetail(id:${props.route.params.roomId}){message{user{id fullname picturePath}message created_at}}}`)
            .then(function (response) {
                // console.log(response);\
                setShowSpinner(false)
                setMessages(response.data.data.chatroomdetail.message)
            }).catch(function (error) {
                setShowSpinner(false)
                console.log(error);
            })
    }

    // Request send message 
    function sendMessageFunction() {
        Keyboard.dismiss();
        const query = `mutation{
            createMessage(input:{chatroom_id:${props.route.params.roomId},user_id:${props.store.userData.id},message:"""${sendMessage}"""}){
              id
              message
            }
        }`

        axios.post(props.store.ip + "/graphql", {
            query: query,
        }, { headers: { 'Content-Type': 'application/json' } }).then(function (response) {
            console.log(response);
            getMessages();
        }).catch(function (error) {
            console.log(error);
        })
    }

    // Refresh content to get latest messages
    function refreshContent() {
        getMessages()
    }

    // Render message using array map.
    function renderMessage() {
        function renderChatBubble(data) {
            console.log(data)
            if (data.user.id == props.store.userData.id) {
                return (
                    <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
                        <View style={{ backgroundColor: "#2c3039", marginTop: 15, borderTopLeftRadius: 20, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, padding: 20 }}>
                            <Text style={{ color: "white" }}>{data.message}</Text>
                        </View>
                        <View style={{ marginLeft: 6 }}>
                            <Image source={{ uri: props.store.ip + "/" + data.user.picturePath, cache: 'reload' }} style={{ width: 50, height: 50, borderRadius: 25 }} />
                        </View>
                    </View>
                )
            } else {
                return (
                    <View style={{ flexDirection: "row" }}>
                        <View style={{ marginRight: 6 }}>
                            <Image source={{ uri: props.store.ip + "/" + data.user.picturePath, cache: 'reload' }} style={{ width: 50, height: 50, borderRadius: 25 }} />
                        </View>
                        <View style={{ backgroundColor: "white", marginTop: 15, borderTopRightRadius: 20, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, padding: 20 }}>
                            <Text>{data.message}</Text>
                        </View>
                    </View>
                )
            }
        }

        return messages.map(function (data, index) {
            // console.log(data);
            return (
                <View key={index} style={{}}>
                    {renderChatBubble(data)}
                </View>
            )
        })
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>

            {/* Activity Indicator */}
            {showSpinnerModal()}

            {/* Header */}
            <View style={styles.headerContainer}>
                <TouchableOpacity onPress={() => props.navigation.goBack()}>
                    <Image source={require("../../../assets/chevron_left_white.png")} style={{ width: 35, height: 35 }} />
                </TouchableOpacity>
                <Text style={{ fontWeight: "bold", color: "white", fontSize: 18 }}>{props.route.params.fullname}</Text>
                <Image source={require("../../../assets/menu_bar_dark.png")} style={{ width: 35, height: 35 }} />
            </View>

            {/* Content Scrollview */}
            <ScrollView
                style={{ backgroundColor: "#dddddd" }}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={() => refreshContent()}
                    />
                }

                keyboardShouldPersistTaps={"always"}>

                {/* Content */}
                <View style={styles.contentContainer}>
                    {renderMessage()}
                </View>

            </ScrollView>


            {/* Footer */}
            <View style={styles.footerContainer}>
                <TextInput placeholder="New message..." multiline={true} style={{ flex: 14 }} onChangeText={(value) => setSendMessage(value)} />
                <TouchableOpacity style={{ flex: 2 }} onPress={() => { sendMessageFunction() }}>
                    <Image source={require("../../../assets/send.png")} style={{ width: 30, height: 30 }} />
                </TouchableOpacity>
            </View>

        </SafeAreaView>
    )
}))

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "space-between",
        alignItems: "center",
    },
    contentContainer: {
        // backgroundColor:"white",
        padding: 10,
        marginTop: 15,
    },
    footerContainer: {
        flexDirection: "row",
        alignItems: "center",
        padding: 10
    }
})

export default ConversationScreen;