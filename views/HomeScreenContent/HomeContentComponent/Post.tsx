import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import axios from 'axios';
import { observer, inject } from "mobx-react";

const Post = inject("store")(observer((props) => {
    const [commentList, setCommentList] = useState([])
    const [commentContent, setCommentContent] = useState("")
    const [showComment, setShowComment] = useState(false)

    // Useeffect hoook as componentdidmount
    useEffect(() => {
        console.log(props);
        getComments();
    }, [])

    // Get list of comments for this post
    function getComments() {
        axios.get(props.store.ip + `/graphql?query={getPostComment(id:${props.postData.id}){comment{content user{fullname picturePath}}}}`)
            .then(function (response) {
                console.log(response);
                setCommentList(response.data.data.getPostComment.comment)
            }).catch(function (error) {
                console.log(error);
            })
    }

    // Render list of comments for this post
    function renderComment() {

        if (showComment == true) {
            let renderCommentList = commentList.map(function (data, index) {
                return (
                    <View style={{ flexDirection: "row", marginTop: index != 0 ? 15 : 0 }} key={index}>
                        <View style={{ marginRight: 6 }}>
                            <Image source={{ uri: props.store.ip + "/" + data.user.picturePath+'?time='+Math.floor(Math.random() * 10), cache: 'reload' }} style={{ width: 40, height: 40, borderRadius: 20 }} />
                        </View>
                        <View style={styles.commentBubble}>
                            <View>
                                <Text style={{ fontWeight: "bold", color: "#2c3039" }}>{data.user.fullname}</Text>
                            </View>
                            <View style={{ marginTop: 5 }}>
                                <Text style={{ lineHeight: 25 }}>{data.content}</Text>
                            </View>
                        </View>
                    </View>
                )
            })

            return (
                <View style={styles.commentContainer}>
                    {/* Render comment list */}
                    {renderCommentList}

                    {/* Reply comment */}
                    <View style={{ flexDirection: "row", marginTop: 20 }}>
                        <View style={{ marginRight: 20, flex: 1, }}>
                            <TouchableOpacity>
                                <Image source={require("../../../assets/camera.png")} style={{ width: 20, height: 20, marginBottom: 10 }} />
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <Image source={require("../../../assets/image.png")} style={{ width: 20, height: 20 }} />
                            </TouchableOpacity>
                        </View>
                        <TextInput placeholder="Write a comment..." style={{ padding: 0, flex: 8 }} multiline={true} onChangeText={(value) => setCommentContent(value)} />
                        <View style={{ flex: 2, justifyContent: "center", alignItems: "center" }}>
                            {/* <TouchableOpacity onPress={() => this.createComment()}> */}
                            <TouchableOpacity>
                                <Image source={require("../../../assets/send.png")} style={{ width: 20, height: 20 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            )
        }
    }

    return (
        <View style={{ width: "90%" }}>

            {/* Post body */}
            <View style={styles.postContainer}>
                <View style={{ marginRight: 6 }}>
                    <Image source={{ uri: props.store.ip + "/" + props.postData.user.picturePath+'?time='+Math.floor(Math.random() * 10), cache: 'reload' }} style={{ width: 40, height: 40, borderRadius: 20 }} />
                </View>
                <View>
                    <Text style={{ fontWeight: "bold", color: "#2c3039" }}>{props.postData.user.fullname}</Text>
                    <View style={{ marginTop: 15 }}>
                        <Text style={{ lineHeight: 25 }}>{props.postData.content}</Text>
                    </View>
                </View>
            </View>

            {/* Footer */}
            <View style={[styles.footerContainer, {
                borderBottomLeftRadius: showComment == false ? 20 : 0,
                borderBottomRightRadius: showComment == false ? 20 : 0,
            }]}>
                <TouchableOpacity onPress={() => { if (showComment == false) { setShowComment(true) } else { setShowComment(false) } }}>
                    <Image source={require("../../../assets/comments.png")} style={{ width: 20, height: 20 }} />
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image source={require("../../../assets/like.png")} style={{ width: 20, height: 20 }} />
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image source={require("../../../assets/share.png")} style={{ width: 20, height: 20 }} />
                </TouchableOpacity>
            </View>

            {/* Comment */}
            {renderComment()}

        </View>
    )
}));

const styles = StyleSheet.create({
    postContainer: {
        backgroundColor: "white",
        padding: 10,
        flexDirection: "row",
        marginTop: 20,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    footerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        justifyContent: "space-evenly",
        padding: 5,
    },
    commentContainer: {
        backgroundColor: "white",
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        padding: 10,
    },
    commentBubble: {
        backgroundColor: "#dddddd",
        padding: 9,
        borderRadius: 20,
    }
})

export default Post;