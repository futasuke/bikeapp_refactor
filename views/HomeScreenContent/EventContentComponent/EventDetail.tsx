import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Image,
    Dimensions,
    Alert,
} from 'react-native';
import axios from 'axios';

// Mobx
import { observer, inject } from "mobx-react";

const EventDetail = inject("store")(observer((props) => {
    const [postData, setPostData] = useState(props.route.params.postData);
    const [attended, setAttended] = useState(false);

    // Useeffect hoook as componentdidmount
    useEffect(() => {
        checkAttend();
    }, [])

    // Check if user already attended the event or not
    function checkAttend() {

        axios.get(props.store.ip + `/graphql?query={joinEvent(user_id:${props.store.userData.id},event_id:${postData.id}){id}}`)
            .then(function (response) {
                if (response.data.data.joinEvent != null) {
                    setAttended(true);
                }
            }).catch(function (error) {
                console.log(error);
            })
    }

    // Request join event
    function joinEvent() {

        axios.get(props.store.ip + `/graphql?query=mutation{joinEvent(user_id:${props.store.userData.id},event_id:${postData.id}){user_id event_id}}`)
            .then(function (response) {
                console.log(response);
                props.navigation.goBack();
            }).catch(function (error) {
                console.log(error);
            })
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView style={{ backgroundColor: "#dddddd" }}>

                {/* Header */}
                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => props.navigation.goBack()}>
                        <Image source={require("../../../assets/chevron_left_white.png")} style={{ width: 35, height: 35 }} />
                    </TouchableOpacity>
                    <Text style={{ fontWeight: "bold", color: "white", fontSize: 18 }}>Event Detail</Text>
                    <TouchableOpacity>
                        <Text style={{ color: "#2c3039", fontWeight: "bold" }}>CREATE</Text>
                    </TouchableOpacity>
                </View>

                {/* Content */}
                <View style={{ marginTop: 20, alignItems: "center" }}>
                    <View style={styles.cardContainer}>
                        <View style={{ marginBottom: 20 }}>
                            <Image source={require("../../../assets/wallpaper/cycling.jpg")} style={{ width: "100%", height: 120 }} borderRadius={20} />
                        </View>

                        <View>
                            <Text style={{ fontSize: 18, fontWeight: "bold" }}>{postData.title}</Text>
                        </View>

                        <View style={{ marginTop: 15 }}>
                            <Text style={{ color: "grey" }}>{postData.description}</Text>
                        </View>

                        <View style={{ marginTop: 15 }}>

                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                                <Image source={require("../../../assets/user_dark.png")} style={{ width: 30, height: 30, marginRight: 10 }} />
                                <Text style={{ fontWeight: "bold", marginRight: 10 }}>Organiser</Text>
                                <Text style={{ marginRight: 10 }}>:</Text>
                                <Text style={{ color: "grey" }}>{postData.user.fullname}</Text>
                            </View>

                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                                <Image source={require("../../../assets/clock.png")} style={{ width: 30, height: 30, marginRight: 10 }} />
                                <Text style={{ fontWeight: "bold", marginRight: 10 }}>Time</Text>
                                <Text style={{ marginRight: 10 }}>:</Text>
                                <Text style={{ color: "grey" }}>{postData.start_time}</Text>
                            </View>

                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                                <Image source={require("../../../assets/calendar_dark.png")} style={{ width: 30, height: 30, marginRight: 10 }} />
                                <Text style={{ fontWeight: "bold", marginRight: 10 }}>Date</Text>
                                <Text style={{ marginRight: 10 }}>:</Text>
                                <Text style={{ color: "grey" }}>{postData.start_date}</Text>
                            </View>

                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                                <Image source={require("../../../assets/dollarsign.png")} style={{ width: 30, height: 30, marginRight: 10 }} />
                                <Text style={{ fontWeight: "bold", marginRight: 10 }}>Fee</Text>
                                <Text style={{ marginRight: 10 }}>:</Text>
                                <Text style={{ color: "grey" }}>{postData.fee}</Text>
                            </View>

                            <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                                <Image source={require("../../../assets/friend_dark.png")} style={{ width: 30, height: 30, marginRight: 10 }} />
                                <Text style={{ fontWeight: "bold", marginRight: 10 }}>Total Participant</Text>
                                <Text style={{ marginRight: 10 }}>:</Text>
                                <Text style={{ color: "grey" }}>{postData.totalParticipant}</Text>
                            </View>
                        </View>
                    </View>
                </View>

                {/* Attend Button */}
                <TouchableOpacity style={styles.attendButton} onPress={() => joinEvent()} disabled={attended}>
                    <Text style={{ color: "white", fontWeight: "bold" }}>{attended ? "Already Attended" : "Attend Event"}</Text>
                </TouchableOpacity>

            </ScrollView>
        </SafeAreaView>
    )
}));

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "space-between",
        alignItems: "center",
    },
    cardContainer: {
        backgroundColor: "white",
        width: "90%",
        borderRadius: 20,
        padding: 10,
    },
    attendButton: {
        width: "90%",
        backgroundColor: "#2c3039",
        alignSelf: "center",
        marginTop: 25,
        padding: 15,
        borderRadius: 20,
        marginBottom: 30,
        alignItems: "center",
        justifyContent: "center",
    }
})

export default EventDetail;