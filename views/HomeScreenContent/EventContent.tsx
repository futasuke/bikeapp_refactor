import React, { useEffect, useState} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity
} from 'react-native';
// import EventPost from './EventContentComponent/EventPost';
import axios from 'axios';
import {observer,inject} from "mobx-react";
import ActivityModal from '../ActivityModal';
import EventPost from './EventContentComponent/EventPost';

const EventContent=inject("store")(observer((props)=>{
    const [allEvent, setAllEvent]=useState([])
    const [showspinner, setShowSpinner] = useState(false);

    useEffect(()=>{
        getEvent()
    },[])

    // Get list of events
    function getEvent(){
        setShowSpinner(true)
        axios.get(props.store.ip+"/graphql?query={eventAll{id,title,description,start_time,start_date,fee,location,user{fullname},totalParticipant}}")
        .then(function(response){
            console.log(response)
            setShowSpinner(false)
            setAllEvent(response.data.data.eventAll)
        }).catch(function(error){
            setShowSpinner(false)
            console.log(error);
        })
    }


    function showSpinnerModal(){
        if(showspinner==true){
            return(
                <ActivityModal />
            )
        }
    }

    // Call Render Component
    // Render each of them using array map
    function renderEventPost(){
        return allEvent.map(function(data,index){
            return(
                <EventPost key={index} postData={data} navigation={props.navigation}/>
            )
        })
    }

    return(
        <View style={{flex:1,alignItems:"center"}}>

            {/* Activity Indicator */}
            {showSpinnerModal()}

            {/* Event Post Content */}
            {renderEventPost()}
        </View>
    )
}));

export default EventContent;