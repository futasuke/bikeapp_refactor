import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    TextInput,
    Image,
    Dimensions,
    Alert,
} from 'react-native';
import axios from 'axios';

// Mobx
import { observer, inject } from "mobx-react";

// Custom import
import ActivityModal from './ActivityModal';

const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

// For upper background purpose
const ratio = screenWidth / 1080;

const LoginScreen = inject("store")(observer((props) => {
    // States
    const [formType, setFormType] = useState("login");
    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [retype_password, setRetypePassword] = useState("");
    const [showspinner, setShowSpinner] = useState(false);

    // Checking inject store and props
    useEffect(() => {
        console.log(props);
    }, [])

    // Show spinner
    function showSpinnerModal() {
        if (showspinner == true) {
            return (
                <ActivityModal />
            )
        }
    }

    // Request login
    function login() {
        console.log("Enter login")

        // Check input if empty
        if (email == "" || password == "") {
            Alert.alert("Please enter all required fields.");
            return;
        } else {
            // let self = this;
            setShowSpinner(true);
            console.log("Before axios: " + props.store.ip + "/api/login")
            axios.post(props.store.ip + "/api/login", {
                email: email,
                password: password
            }).then(function (response) {
                console.log(response)
                setShowSpinner(false);
                if (response.data.errors) {
                    console.log("error");
                    return;
                } else {
                    console.log("success");
                    props.store.userData = response.data.data;
                    props.navigation.navigate("HomeDrawer");
                    return;
                }
            }).catch(function (error) {
                console.log(error)
                setShowSpinner(false);
            }).finally(() => {
                setShowSpinner(false);
                console.log("Does not work");
            })
        }
    }

    // Request to register user
    function register() {
        // Check state not empty
        if (email == "" || password == "" || retype_password == "") {
            Alert.alert("Please enter all required fields.");
            return;
        } else if (password != retype_password) {
            Alert.alert("Password and retype password is not the same.");
            return;
        } else {
            // let self = this;
            setShowSpinner(true);

            axios.get(props.store.ip + `/graphql?query=mutation{createUser(username:"${username}",email:"${email}",password:"${password}",fullname:"${username}"){id,username,email,fullname,picturePath}}`)
                .then(function (response) {
                    console.log(response);
                    setShowSpinner(false);
                    if (response.data.errors) {
                        console.log("error");
                        return;
                    } else {
                        console.log("success");
                        props.store.userData = response.data.data.createUser
                        props.navigation.navigate("HomeDrawer");
                    }
                }).catch(function (error) {
                    console.log(error);
                    setShowSpinner(false);
                })
        }
        // this.props.navigation.navigate("HomeDrawer");
    }

    // Render body content for login screen
    function renderContent() {
        if (formType == "login") {
            return (
                <View style={{ width: "100%", alignItems: "center" }}>
                    <View style={[styles.inputContainer]}>
                        <Image source={require("../assets/email.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Email" style={{ padding: 0, flex: 13 }} onChangeText={value => setEmail(value)} />
                    </View>
                    <View style={[styles.inputContainer, { marginTop: 20 }]}>
                        <Image source={require("../assets/password.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Password" style={{ padding: 0, flex: 13 }} onChangeText={value => setPassword(value)} />
                    </View>
                    <TouchableOpacity style={[styles.submitButton, { marginTop: 30 }]} onPress={() => { login() }}>
                        {/* <TouchableOpacity style={[styles.submitButton, { marginTop: 30 }]}> */}
                        <Text style={{ color: "white", fontWeight: "bold" }}>LOGIN</Text>
                    </TouchableOpacity>
                </View>
            )
        }
        if (formType == "register") {
            return (
                <View style={{ width: "100%", alignItems: "center" }}>
                    <View style={[styles.inputContainer]}>
                        <Image source={require("../assets/email.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Email" style={{ padding: 0, flex: 13 }} onChangeText={value => setEmail(value)} />
                    </View>
                    <View style={[styles.inputContainer, { marginTop: 20 }]}>
                        <Image source={require("../assets/user_dark.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Username" style={{ padding: 0, flex: 13 }} onChangeText={value => setUsername(value)} />
                    </View>
                    <View style={[styles.inputContainer, { marginTop: 20 }]}>
                        <Image source={require("../assets/password.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Password" style={{ padding: 0, flex: 13 }} onChangeText={value => setPassword(value)} />
                    </View>
                    <View style={[styles.inputContainer, { marginTop: 20 }]}>
                        <Image source={require("../assets/password.png")} style={{ width: 30, height: 30, marginRight: 20, flex: 2 }} />
                        <TextInput placeholder="Re-enter Password" style={{ padding: 0, flex: 13 }} onChangeText={value => setRetypePassword(value)} />
                    </View>
                    <TouchableOpacity style={[styles.submitButton, { marginTop: 30 }]} onPress={() => { register() }}>
                    {/* <TouchableOpacity style={[styles.submitButton, { marginTop: 30 }]} > */}
                        <Text style={{ color: "white", fontWeight: "bold" }}>REGISTER</Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }

    // Main return
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView style={{ backgroundColor: "#dddddd" }}>

                {/* Activity indicator */}
                {showSpinnerModal()}

                <View style={styles.upperPart}>
                    <Image source={require("../assets/loginbg_upper.png")} style={{ width: screenWidth, height: 736 * ratio }} resizeMode={"contain"} />
                </View>

                <View style={styles.lowerPart}>
                    <View style={styles.tabContainer}>
                        <TouchableOpacity onPress={() => setFormType("login")}>
                            <Text style={{ fontWeight: "bold" }}>LOGINA</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => setFormType("register")}>
                            <Text style={{ fontWeight: "bold" }}>REGISTER</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.contentContainer}>
                        {renderContent()}
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}));

const styles = StyleSheet.create({
    upperPart: {
        backgroundColor: "#2c3039",
    },
    lowerPart: {
        backgroundColor: "#dddddd",
        marginBottom: 20,
    },
    tabContainer: {
        height: 50,
        // backgroundColor: "red",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-evenly",
        borderBottomColor: "#2c3039",
        borderBottomWidth: 1,
    },
    contentContainer: {
        flex: 9,
        paddingTop: 50,
        // backgroundColor:"blue"
    },
    inputContainer: {
        backgroundColor: "white",
        width: "90%",
        borderRadius: 20,
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        alignItems: "center",
    },
    submitButton: {
        backgroundColor: "#2c3039",
        width: "90%",
        borderRadius: 20,
        padding: 15,
        justifyContent: "center",
        alignItems: "center",
    }
})

export default LoginScreen;