import React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    Dimensions,
    Modal,
    ActivityIndicator
} from 'react-native';

const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

const ActivityModal = () => {
    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={true}
        >
            <View style={{ height: screenHeight, width: screenWidth, backgroundColor: "rgba(0,0,0,0.4)", justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ backgroundColor: 'white', width: 150, height: 150, justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}>
                    <ActivityIndicator size="large" animating={true} color="#2c3039" />
                </View>
            </View>
        </Modal>
    )
}

export default ActivityModal;