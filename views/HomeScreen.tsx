import 'react-native-gesture-handler';
import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Image
} from 'react-native';

// HomeScreen Content
import HomeContent from './HomeScreenContent/HomeContent';
import EventContent from './HomeScreenContent/EventContent';
import MapContent from './HomeScreenContent/MapContent';
import MessageContent from './HomeScreenContent/MessageContent';

const HomeScreen = (props) => {
    const [contentType, setContentType] = useState("home");

    // Render content dynamically for home
    function renderContent() {
        if (contentType == "home") {
            return (
                <HomeContent navigation={props.navigation} />
            )
        }
        if (contentType == "map") {
            return (
                <MapContent />
            )
        }
        if (contentType == "message") {
            return (
                <MessageContent navigation={props.navigation} />
            )
        }
        if (contentType == "event") {
            return (
                <EventContent navigation={props.navigation} />
            )
        }
    }

    // Render add button at bottom right 
    function renderAddButton() {
        if (contentType == "home") {
            return (
                <TouchableOpacity style={styles.addButtonContent} onPress={() => goToNewPost()}>
                    <Image source={require("../assets/pencil.png")} style={{ width: 20, height: 20 }} />
                </TouchableOpacity>
            )
        }
        if (contentType == "event") {
            return (
                <TouchableOpacity style={styles.addButtonContent} onPress={() => goToNewEvent()}>
                    <Image source={require("../assets/calendar_plus.png")} style={{ width: 20, height: 20 }} />
                </TouchableOpacity>
            )
        }
    }

    // Go to create new event
    function goToNewEvent() {
        props.navigation.navigate("CreateEvent");
    }

    // Go to create new post
    function goToNewPost() {
        props.navigation.navigate("CreatePost");
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView style={{ backgroundColor: "#dddddd" }}>

                {/* Header */}
                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => props.navigation.toggleDrawer()}>
                        <Image source={require("../assets/menu_bar.png")} style={{ width: 35, height: 35 }} />
                    </TouchableOpacity>
                    <Text style={{ fontWeight: "bold", color: "white", fontSize: 18 }}>Bike App</Text>
                    <Image source={require("../assets/menu_bar_dark.png")} style={{ width: 35, height: 35 }} />
                </View>

                {/* Content */}
                <View style={styles.body}>
                    {renderContent()}
                </View>

            </ScrollView>

            {/* Footer */}
            <View style={styles.footer}>
                <TouchableOpacity onPress={() => setContentType("home")}>
                    <Image source={require("../assets/home_white.png")} style={{ width: 35, height: 35 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setContentType("event")}>
                    <Image source={require("../assets/calendar.png")} style={{ width: 35, height: 35 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setContentType("map")}>
                    <Image source={require("../assets/map_white.png")} style={{ width: 35, height: 35 }} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setContentType("message")}>
                    <Image source={require("../assets/message_white.png")} style={{ width: 35, height: 35 }} />
                </TouchableOpacity>
            </View>

            {/* Add Button */}
            <View style={styles.addButtonContainer}>
                {renderAddButton()}
            </View>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "space-between",
        alignItems: "center",
    },
    body: {
        marginBottom: 80,
    },
    footer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-evenly",
        paddingBottom: 6,
        paddingTop: 6,
        // position:"absolute",
        // bottom:0,
    },
    addButtonContainer: {
        position: "absolute",
        bottom: 80,
        right: 10,
    },
    addButtonContent: {
        backgroundColor: "#2c3039",
        borderRadius: 20,
        padding: 15,
    }
})

export default HomeScreen;