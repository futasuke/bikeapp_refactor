import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
    TouchableOpacity,
    Dimensions,
    ImageBackground,
} from 'react-native';
import { observer, inject } from "mobx-react";
import { TextInput } from 'react-native-gesture-handler';
import axios from 'axios';
import { values } from 'mobx';
import { launchImageLibrary } from 'react-native-image-picker';

const EditProfile = inject("store")(observer((props) => {
    const [email, setEmail] = useState(props.store.userData.email)
    const [username, setUsername] = useState(props.store.userData.username)
    const [fullname, setFullname] = useState(props.store.userData.fullname)
    const [id, setId] = useState(props.store.userData.id)
    const [base64, setBase64] = useState("")
    const [filetype, setFileType] = useState("")

    useEffect(() => {
        console.log(props);
    }, [])

    // Open phone gallery to choose picture
    function openGallery() {
        const options = {
            mediaType: 'photo',
            includeBase64: true,
        }

        launchImageLibrary(options, (response) => {
            console.log(response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
                return;
            }
            let tmpType = response.type.split("/");
            console.log(tmpType);
            setFileType(tmpType[1]);
            setBase64(response.base64);
            uploadImage(response.base64, tmpType[1]);
        })
    }

    // Upload the image to backend using base64
    function uploadImage(base64Pass:String, fileType:String) {
        console.log("uploadimage");
        // console.log();
        axios.post(props.store.ip + "/api/upload-profile-image", {
            base64: base64Pass,
            id: id,
            filetype: fileType
        }).then(function (response) {
            updateUser();
        }).catch(function (error) {
            console.log(error.response);
        })

    }

    // Request update user
    function updateUser() {

        axios.get(props.store.ip + `/graphql?query=mutation{updateUser(id:${id},username:"${username}",fullname:"${fullname}",email:"${email}"){id,username,email,fullname,picturePath}}`)
            .then(function (response) {
                console.log(response)
                if (response.data.errors) {
                    console.log("error");
                    return;
                } else {
                    console.log("success");
                    props.store.userData = response.data.data.updateUser;
                    props.navigation.goBack();
                }
            }).catch(function (error) {
                console.log(error)
            })
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView style={{ backgroundColor: "#dddddd" }}>

                {/* Header */}
                <View style={styles.headerContainer}>
                    <TouchableOpacity onPress={() => props.navigation.goBack()}>
                        <Image source={require("../../assets/chevron_left_white.png")} style={{ width: 35, height: 35 }} />
                    </TouchableOpacity>
                    <Text style={{ fontWeight: "bold", color: "white", fontSize: 18 }}>Edit Profile</Text>
                    <Image source={require("../../assets/menu_bar_dark.png")} style={{ width: 35, height: 35 }} />
                </View>

                {/* Body */}
                <View>
                    {/* Profile Picture */}
                    <View style={styles.upperPart}>
                        <Image source={{ uri: props.store.ip + "/" + props.store.userData.picturePath+'?time='+Math.floor(Math.random() * 10), cache: 'reload' }} style={{ width: 100, height: 100, borderRadius: 50 }} />
                        <TouchableOpacity onPress={() => openGallery()}>
                            <Text style={{ fontWeight: "bold", color: "white" }}>Change Picture</Text>
                        </TouchableOpacity>
                    </View>

                    {/* Profile Data */}
                    <View style={styles.lowerPart}>

                        <View style={styles.cardContainer}>
                            <Text style={{ fontWeight: "bold" }}>Email :</Text>
                            <TextInput style={{ color: "grey", padding: 0 }} defaultValue={email} onChangeText={value => setEmail(value)} />
                        </View>

                        <View style={styles.cardContainer}>
                            <Text style={{ fontWeight: "bold" }}>Username :</Text>
                            <TextInput style={{ color: "grey", padding: 0 }} defaultValue={username} onChangeText={value => setUsername(value)} />
                        </View>

                        <View style={styles.cardContainer}>
                            <Text style={{ fontWeight: "bold" }}>Full Name :</Text>
                            <TextInput style={{ color: "grey", padding: 0 }} defaultValue={fullname} onChangeText={value => setFullname(value)} />
                        </View>

                        <TouchableOpacity style={styles.submitButton} onPress={() => updateUser()}>
                            <Text style={{ fontWeight: "bold", color: "white" }}>UPDATE</Text>
                        </TouchableOpacity>

                    </View>
                </View>

            </ScrollView>
        </SafeAreaView>
    )
}))

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: "#2c3039",
        flexDirection: "row",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        justifyContent: "space-between",
        alignItems: "center",
    },
    upperPart: {
        backgroundColor: "#2c3039",
        justifyContent: "center",
        alignItems: "center",
        paddingBottom: 20,
    },
    lowerPart: {
        alignItems: "center",
        // paddingTop:20,
    },
    cardContainer: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: "white",
        width: "90%",
        borderRadius: 20,
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 20,
        alignItems: "center"
    },
    submitButton: {
        width: "90%",
        backgroundColor: "#2c3039",
        marginTop: 40,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20,
        padding: 20,
    }
})

export default EditProfile;