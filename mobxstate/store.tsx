import {observable} from 'mobx'

class AppStore{
    @observable ip:String = "http://192.168.0.105:8080"
    @observable userData = [];
    @observable postData = [];
}

export default AppStore