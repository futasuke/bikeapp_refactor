import 'react-native-gesture-handler';
import React, { useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions
} from 'react-native';

// React navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

// MobX
import { Provider } from "mobx-react";
import AppStore from './mobxstate/store';
const store = window.store = new AppStore();

// Views/Screens
import HomeScreen from './views/HomeScreen';
import SettingScreen from './views/SettingScreen';
import LoginScreen from './views/LoginScreen';
import EventDetail from './views/HomeScreenContent/EventContentComponent/EventDetail';
import SidebarContent from './views/SidebarContent/SidebarContent';
import EditProfile from './views/SettingScreenContent/EditProfile';
import ConversationScreen from './views/HomeScreenContent/MessageContentComponent/ConversationScreen';
import CreatePost from './views/HomeScreenContent/HomeContentComponent/CreatePost';
import CreateEvent from './views/HomeScreenContent/EventContentComponent/CreateEvent';

const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;
const MainStack = createStackNavigator();
const MainDrawer = createDrawerNavigator();

const App = () => {

  function mainDrawer(props) {
    const [thisRoute, setThisRoute]=useState(props.route)

    return (
      <Provider store={store}>
        <MainDrawer.Navigator drawerContent={props => { return (<SidebarContent navigation={props.navigation} route={thisRoute} store={store}/>) }} drawerStyle={{ width: screenWidth * 0.8 }}>
          <MainDrawer.Screen name="Home" component={HomeScreen} />
          <MainDrawer.Screen name="Setting" component={SettingScreen} />
        </MainDrawer.Navigator>
      </Provider>
    )
  }

  return (
    <Provider store={store}>
      <NavigationContainer>
        <MainStack.Navigator headerMode="none">
          <MainStack.Screen name="Login" component={LoginScreen} />
          <MainStack.Screen name="HomeDrawer" children={mainDrawer} />
          <MainStack.Screen name="EventDetail" component={EventDetail} />
          <MainStack.Screen name="EditProfile" component={EditProfile} />
          <MainStack.Screen name="ConversationScreen" component={ConversationScreen} />
          <MainStack.Screen name="CreatePost" component={CreatePost} />
          <MainStack.Screen name="CreateEvent" component={CreateEvent} />
        </MainStack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

export default App;
